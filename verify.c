/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :    1                                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   Analyse de trame GPS                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : LAVAL Mélina                                                 *
*                                                                             *
*  Nom-prénom2 :   			                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  verify.c                                                 *
*                                                                             *
******************************************************************************/

//verify ne possède pas de .h car ses signatures sont répétoriées dans le gps.h pour plus de simplicité 
#include "gps.h"

int verifyTrameStruct(char *trame, char **values) {
    	int count = 0;

    	char* cpyTrame = malloc(strlen(trame));
    	if(cpyTrame == NULL) {
        	return EXIT_FAILURE;
    	}
    	strcpy(cpyTrame, trame);

    	while (1) {
        	char *cpy = strchr(cpyTrame, ',');
        	if(cpy == NULL) break;
        	values[count] = cpy;
        	//printf("(%d) %s\n", count, cpy);
        	cpyTrame = cpy+1;
        	count++;
    	}

    	if(count < 14) {
        	return EXIT_FAILURE;
    	}
    	return EXIT_SUCCESS;
}

int verifyTrameType(char *trame) {
    	char type[7];
    	strncpy(type, trame, 6);

    	char goodType[] = "$GPGGA";
    	for(int i = 0; i < 6; i ++) {
        	if(type[i] != goodType[i]) {
            		return EXIT_FAILURE;
        	}
    	}

    	return EXIT_SUCCESS;
}
