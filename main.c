/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :    1                                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   Analyse de trame GPS                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : LAVAL Mélina                                                 *
*                                                                             *
*  Nom-prénom2 :   			                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                   *
*                                                                             *
******************************************************************************/

#include "gps.h"
#include "verify.h"

void main() {
	// exemple de trame
    	//char *trameString = "$GPGGA,064036.289,4838.5375,N,08151.6838,E,1,04,3.2,200.2,M,,,,0000*0E";
    	char *trameString = malloc(500);

   	printf("Entrez une trame gps $GPGGA: \n");
    	fgets(trameString, 1024, stdin);

    	Trame trame;
    	((Trame *) &trame)->values = malloc(500);

	// verification 
   	if(verifyTrameStruct(trameString, trame.values) == 1) {
        	perror("La structure de la trame n'est pas bonne !\n");
        	return;
   	}
	
	// verification 
	if(verifyTrameType(trameString) == 1) {
        	perror("Le type de la trame n'est pas valide !\n");
        	return;
    	}

    	printf("\nInformations sur la trame:\n");
    	
    	// obtention heure minute seconde
    	buildHours(&trame);
    	Temps temps = trame.temps;
    	printf("%sh%sm%ss\n", temps.heure, temps.minute, temps.seconde);

	// obtention latitude et longitude
    	buildLatitude(&trame);
    	buildLongitude(&trame);

    	printf("Longitude: %s\n", trame.longitude);
    	printf("Latitude: %s\n", trame.latitude);
    
    	printf("Sauvegarde des informations dans un fichier texte\n");		
    	FILE* fichier = NULL;
	fichier = fopen("sauvegarde.txt", "a");
	
	//affichage des différentes informations obtenues par la trame dans le fichier texte			
	fprintf(fichier,"\n---------------------------\n");
    	fprintf(fichier," INFORMATIONS SAUVEGARDEES\n");
	fprintf(fichier,"---------------------------\n");
   	fprintf(fichier,"\nInformations sur la trame:\n");
   	fprintf(fichier,"\nTrame : %s\n",trameString);
   	fprintf(fichier,"%sh%sm%ss\n", temps.heure, temps.minute, temps.seconde);
    	fprintf(fichier,"Longitude: %s\n", trame.longitude);
    	fprintf(fichier,"Latitude: %s\n", trame.latitude);
    	
    	//On libère la mémoire 
    	free(trame.values);
    	free(trameString);
}
