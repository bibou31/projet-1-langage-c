/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :    1                                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   Analyse de trame GPS                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : LAVAL Mélina                                                 *
*                                                                             *
*  Nom-prénom2 :   			                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  gps.c                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gps.h"

void buildHours(Trame *trame) {
	char **values = trame->values;
    	Temps  *temps = &trame->temps;

    	char heureAll[13];
    	strncpy(heureAll,(values[0]+1), values[1]-values[0]);

    	strncpy(temps->heure, heureAll, 2);
    	strncpy(temps->minute, heureAll+2, 2);
    	strncpy(temps->seconde, heureAll+4, 2);

    	temps->heure[2] = '\0';
    	temps->minute[2] = '\0';
    	temps->seconde[2] = '\0';
}

void buildLatitude(Trame *trame) {
    char **values = trame->values;

    char lat[13];
    strncpy(lat,(values[1]+1), values[3]-values[1]);

    char deg[3], deg2[3], deg3[5], all[10] = "";

    strncpy(deg, lat, 2);
    strncpy(deg2, lat+2, 2);
    strncpy(deg3, lat+5, 4);

    deg[2] = '\0';
    deg2[2] = '\0';
    deg3[4] = '\0';

    int val = (atoi(deg3) * 0.0001)*60;
    char str[3];
    sprintf(str, "%d", val);
    str[2] = '\0';

    strcat(all, deg);
    strcat(all, "d");
    strcat(all, deg2);
    strcat(all, "'");
    strcat(all, str);
    strcat(all, "\"");
    strcpy(trame->latitude, all);
}

void buildLongitude(Trame *trame) {
    char **values = trame->values;

    char log[13];
    strncpy(log, (values[3] + 2), values[5] - values[3]);

    char deg[3], deg2[3], deg3[5], all[10] = "";

    strncpy(deg, log, 2);
    strncpy(deg2, log + 2, 2);
    strncpy(deg3, log + 5, 4);

    deg[2] = '\0';
    deg2[2] = '\0';
    deg3[4] = '\0';

    int val = (atoi(deg3) * 0.0001)*60;
    char str[3];
    sprintf(str, "%d", val);
    str[2] = '\0';

    strcat(all, deg);
    strcat(all, "d");
    strcat(all, deg2);
    strcat(all, "'");
    strcat(all, str);
    strcat(all, "\"");
    strcpy(trame->longitude, all);
}
