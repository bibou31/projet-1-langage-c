/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet :    1                                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :   Analyse de trame GPS                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : LAVAL Mélina                                                 *
*                                                                             *
*  Nom-prénom2 :   			                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  gps.h                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Temps {
    	char heure[3], minute[3], seconde[3];
};

typedef struct Temps Temps;

struct Trame {
    	Temps temps;
    	char latitude[10], longitude[10];
    	char **values;
};

typedef struct Trame Trame;

//void initTrame(Trame *trame);

int verifyTrameStruct(char *trame, char **values);
int verifyTrameType(char *trame);

void buildHours(Trame *trame);
void buildLatitude(Trame *trame);
void buildLongitude(Trame *trame);
