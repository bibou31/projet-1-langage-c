# projet 1 langage C

LAVAL Mélina

Analyse de trame GPS

L'application sert à nous founir les informations provenant de trames GPS suivant la norme NMEA 0183. Comme afficher l'heure, la longitude et la latitude, le tout étant sauvegardé dans un fichier texte.

Pour utiliser l'application faire la commande [make all] puis après l'utilisation faire [make clean] afin de supprimer les fichiers annexes et la sauvegarde des informations contenues dans sauvegarde.txt .

L'ensemble des signatures :

void buildHours(Trame *trame);
	// extrait le temps ( heure, minute, seconde ) de la trame à partir du pointeur en entrée

void buildLatitude(Trame *trame);
	// extrait la latitude de la trame à partir du pointeur en entrée

void buildLongitude(Trame *trame);
	// extrait la latitude de la trame à partir du pointeur en entrée

int verifyTrameStruct(char *trame, char **values);
	// vérifie que la trame soit de la bonne structure à partir d'un pointeur et d'un tableau de pointeurs ( non null et > 14 char ) renvoie EXIT_SUCCESS si elle est de la bonne structure sinon EXIT_FAILURE

int verifyTrameType(char *trame);
	// vérifie que la trame soit du type "$GPGGA" à partir du pointeur en entrée et renvoie EXIT_SUCCESS si elle est du bon type sinon EXIT_FAILURE


Les cas d'erreurs : 
	-La trame n'est pas du type "$GPGGA"
	-La trame est vide
	-La trame est inférieur à 14 char ( seuleument les informations qui nous intéresses)
  
